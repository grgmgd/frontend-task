import { spring } from "react-flip-toolkit";

export const onExit = (
  el: HTMLElement,
  index: number,
  removeElement: () => void
) => {
  spring({
    onUpdate: (val) => {
      el.style.transform = `scale(${1 - (val as number)})`;
      el.style.opacity = `${1 - (val as number)}`;
      el.style.transitionDuration = "0.02s";
    },
    delay: 0,

    onComplete: () => {
      el.style.opacity = "";
      el.style.transform = "rotate(0)";
      removeElement();
    },
  });

  return () => {
    el.style.opacity = "";
    el.style.transform = "rotate(0)";
    removeElement();
  };
};

export const onAppear = (el: HTMLElement, index: number) =>
  spring({
    onUpdate: (val) => {
      el.style.transform = `scale(${val})`;
      el.style.opacity = `${val}`;
    },
    delay: index * 50,
  });

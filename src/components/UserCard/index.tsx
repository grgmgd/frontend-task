import { useState } from "react";

import InterestTag from "../InterestTag";
import { Flipped } from "react-flip-toolkit";

import { ReactComponent as ClearIcon } from "../../assets/clear.svg";
import { onAppear, onExit } from "./animationsConfig";
import { TUser } from "../../models/user";
import { UserEntities } from "../../utils/normalizeUsers";

interface Props {
  user: string;
  onUserDelete: (id: number) => void;
  entities: UserEntities;
  index: number;
}

const UserCard = ({ user: id, entities, onUserDelete }: Props) => {
  const user: TUser = entities[id];
  const [openInterests, setOpenInterests] = useState(false);
  const [interests, setInterests] = useState(user.interests ?? []);

  const onDeleteClick = () => {
    onUserDelete(user.id);
  };

  const onInterestsClick = () => {
    setOpenInterests((current) => !current);
  };

  const onInterestDelete = (id: number) => {
    setInterests((current) => current.filter((interest) => interest !== id));
  };

  const flipKey = `user-${user.id}`;

  return (
    <Flipped
      transformOrigin="center top"
      onExit={onExit}
      onAppear={onAppear}
      flipId={flipKey}
      key={flipKey}
    >
      <div className="flex flex-row p-5 bg-white shadow-2xl rounded-xl">
        <div className="mr-5 bg-gray-900 border-2 border-gray-200 border-solid min-w-[4rem] min-h-[4rem] h-16 rounded-xl bg-opacity-20" />
        <div className="flex-1">
          <h1 className="mb-2 font-bold">{user.name}</h1>

          <p className="text-sm text-gray-700">
            Has {user.followers} followers
          </p>
          <div className="mt-5">
            {interests.length > 0 && (
              <>
                <button
                  className={`${
                    openInterests ? "bg-gray-400" : "bg-transparent"
                  } px-2 py-2 text-xs text-gray-800 transition-all duration-150 ease-in-out rounded cursor-pointer bg-opacity-30 hover:bg-opacity-30 hover:bg-gray-400`}
                  onClick={onInterestsClick}
                >
                  View Interests
                </button>
                <div className="flex flex-row flex-wrap gap-2 mt-2">
                  {openInterests &&
                    interests.map((interest) => (
                      <InterestTag
                        key={`interest_${interest}`}
                        onInterestDelete={onInterestDelete}
                        id={interest}
                      />
                    ))}
                </div>
              </>
            )}
            {interests.length === 0 && (
              <p className="text-xs text-gray-400">
                {user.name} has no interests
              </p>
            )}
          </div>
        </div>
        <div className="flex items-start justify-center">
          <div
            onClick={onDeleteClick}
            className="w-10 h-10 p-3 transition-all duration-150 ease-in-out rounded-lg cursor-pointer hover:bg-opacity-30 hover:bg-gray-400"
          >
            <ClearIcon className="w-full h-full" />
          </div>
        </div>
      </div>
    </Flipped>
  );
};

export default UserCard;

import { memo, useContext } from "react";
import InterestsContext from "../utils/InterestsContext";
import { ReactComponent as ClearIcon } from "../assets/clear.svg";

interface Props {
  id: number;
  onInterestDelete: (id: number) => void;
}

const InterestTag = ({ id, onInterestDelete }: Props) => {
  const { interests } = useContext(InterestsContext);

  const onDeleteClick = () => {
    onInterestDelete(id);
  };

  return (
    <div className="flex flex-row h-8 px-4 py-1 text-sm text-white bg-green-500 border-2 border-green-400 border-solid rounded select-none  interest group">
      {interests[id].name}

      <div
        onClick={onDeleteClick}
        className="flex items-center justify-center w-0 m-0 overflow-hidden transition-all duration-200 ease-in-out cursor-pointer group-hover:ml-2 group-hover:w-3"
      >
        <ClearIcon className="w-3 h-3" />
      </div>
    </div>
  );
};

export default memo(InterestTag);

import InterestsContext from "./utils/InterestsContext";
import useUserData from "./hooks/useUserData";
import useInterestData from "./hooks/useInterestData";

import Users from "./views/Users";

function App() {
  const { userData, loading: loadingUsers } = useUserData();
  const { interestData, loading: loadingInterests } = useInterestData();

  return (
    <InterestsContext.Provider value={{ interests: interestData }}>
      <div className="w-screen h-screen overflow-scroll bg-gradient-to-bl from-gray-900 to-gray-700">
        {(loadingUsers || loadingInterests) && (
          <div className="mx-auto mt-10 font-bold text-center text-white animate-pulse">
            Loading
          </div>
        )}
        {!loadingUsers && !loadingInterests && <Users users={userData} />}
      </div>
    </InterestsContext.Provider>
  );
}

export default App;

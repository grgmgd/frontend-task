import { useEffect, useState } from "react";
import normalizeInterests, {
  InterstEntities,
} from "../utils/normalizeInterests";

const useInterestData = () => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState<InterstEntities>({});

  useEffect(() => {
    setTimeout(() => {
      setData(normalizeInterests(require("../data/interests.json")).entities);
      setLoading(false);
    }, 2000);
  }, []);

  return { interestData: data, loading };
};

export default useInterestData;

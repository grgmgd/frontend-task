import { useEffect, useState } from "react";
import normalizeUsers, { TNormalizedUsers } from "../utils/normalizeUsers";

const useUserData = () => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState<TNormalizedUsers>({
    entities: {},
    ids: [],
  });

  useEffect(() => {
    setTimeout(() => {
      setData(normalizeUsers(require("../data/users.json")));
      setLoading(false);
    }, 2000);
  }, []);

  return { userData: data, loading };
};

export default useUserData;

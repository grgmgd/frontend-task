import { TInterest } from "../models/intreset";

export type InterstEntities = {
  [id: string]: TInterest;
};

const normalizeInterests = (interests: TInterest[]) => ({
  ids: interests.map((interest) => interest.id),
  entities: interests.reduce((acc: InterstEntities, interest) => {
    acc[interest.id] = interest;
    return acc;
  }, {}),
});

export default normalizeInterests;

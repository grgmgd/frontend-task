import { createContext } from "react";
import { InterstEntities } from "./normalizeInterests";

const InterestsContext = createContext<{ interests: InterstEntities }>({
  interests: {},
});

export default InterestsContext;

import { TUser } from "../models/user";

export type UserEntities = {
  [id: string]: TUser;
};

export type TNormalizedUsers = {
  entities: UserEntities;
  ids: string[];
};

const normalizeUsers = (users: TUser[]): TNormalizedUsers => {
  const entities = users.reduce((acc: UserEntities, user) => {
    user.followers = 0;
    acc[user.id] = user;
    return acc;
  }, {});

  Object.keys(entities).forEach((key) => {
    entities[key].following.forEach((id: number) => {
      if (entities[id]) entities[id].followers += 1;
    });
  });

  const ids = Object.keys(entities).sort((a, b) =>
    entities[a].followers > entities[b].followers ? -1 : 1
  );

  return { entities, ids };
};

export default normalizeUsers;

export type TUser = {
  id: number;
  name: string;
  following: number[];
  interests?: number[];
  followers: number;
};

export type TInterest = {
  id: number;
  name: string;
};

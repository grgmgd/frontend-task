import { useState } from "react";
import { Flipper } from "react-flip-toolkit";
import UserCard from "../components/UserCard";
import normalizeUsers, { TNormalizedUsers } from "../utils/normalizeUsers";

interface Props {
  users: TNormalizedUsers;
}

const Users = ({ users: initialUserData }: Props) => {
  const [users, setUsers] = useState(initialUserData);

  const onUserDelete = (id: number) => {
    setUsers((current) => {
      return normalizeUsers(
        Object.values(current.entities).filter((user) => user.id !== id)
      );
    });
  };

  return (
    <div className="container max-w-[800px] flex flex-col gap-10 px-5 py-10 mx-auto">
      <h1 className="font-bold text-white">Users</h1>
      <Flipper
        spring="stiff"
        applyTransformOrigin={false}
        flipKey={users.ids.join("")}
      >
        <div className="flex flex-col gap-8">
          {users.ids?.map((user, index) => (
            <UserCard
              onUserDelete={onUserDelete}
              key={`user_${user}`}
              {...{ user, index }}
              entities={users.entities}
            />
          ))}
        </div>
      </Flipper>
    </div>
  );
};

export default Users;
